package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.myapplication.precedence.adapter.ListViewAdapter;
import com.example.myapplication.precedence.http.ApiServiceSigleton;
import com.example.myapplication.precedence.http.PrecedenceViewModel;
import com.example.myapplication.precedence.model.PrecedenceModel;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SecondActivity extends AppCompatActivity {

    private final String TAG="sa_ac";

    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView name=findViewById(R.id.sa_tv_name);
        TextView situation=findViewById(R.id.sa_tv_situ);
        TextView bVol=findViewById(R.id.sa_tv_bVol);
        TextView comp=findViewById(R.id.sa_tv_comp);
        TextView indust=findViewById(R.id.sa_tv_indus);

        TextView update=findViewById(R.id.sa_tv_update);
        TextView num=findViewById(R.id.sa_tv_num);
        TextView latest=findViewById(R.id.sa_tv_latest);
        TextView vol=findViewById(R.id.sa_tv_Vol);
        TextView finalP=findViewById(R.id.sa_tv_final);
        TextView val=findViewById(R.id.sa_tv_val);
        TextView permitted=findViewById(R.id.sa_tv_permitted);
        TextView first=findViewById(R.id.sa_tv_first);
        TextView period=findViewById(R.id.sa_tv_period);
        TextView yester=findViewById(R.id.sa_tv_yester);

        TextView maxW=findViewById(R.id.sa_maxW);
        TextView aveValueW=findViewById(R.id.sa_aveValW);
        TextView minW=findViewById(R.id.sa_minW);
        TextView aveVolW=findViewById(R.id.sa_aveVolW);

        TextView maxM=findViewById(R.id.sa_maxM);
        TextView aveValueM=findViewById(R.id.sa_aveValM);
        TextView minM=findViewById(R.id.sa_minM);
        TextView aveVolM=findViewById(R.id.sa_aveVolM);

        TextView max3=findViewById(R.id.sa_max3m);
        TextView aveValue3=findViewById(R.id.sa_aveVal3m);
        TextView min3=findViewById(R.id.sa_min3m);
        TextView aveVol3=findViewById(R.id.sa_aveVol3m);

        TextView max6=findViewById(R.id.sa_max6m);
        TextView aveValue6=findViewById(R.id.sa_aveVal6m);
        TextView min6=findViewById(R.id.sa_min6m);
        TextView aveVol6=findViewById(R.id.sa_aveVol6m);

        TextView maxY=findViewById(R.id.sa_maxY);
        TextView aveValueY=findViewById(R.id.sa_aveValY);
        TextView minY=findViewById(R.id.sa_minY);
        TextView aveVolY=findViewById(R.id.sa_aveVolY);



        String extraName;
        Bundle extras=getIntent().getExtras();
        extraName=extras.getString("name");
        Log.i(TAG, "onCreate: "+extraName);

        PrecedenceViewModel precedenceViewModel=new PrecedenceViewModel(ApiServiceSigleton.getInstance(),"precedence.json");


        precedenceViewModel.getStocks().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<PrecedenceModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onSuccess(List<PrecedenceModel> precedenceModels) {
                        for (int i = 0; i < precedenceModels.size(); i++) {
                            Log.i(TAG, "onSuccess: "+precedenceModels.get(i).getName());
                            if (precedenceModels.get(i).getName().equals(extraName.toString())){
//                                precedenceModels.get(i).getName().equals(extraName){

                                PrecedenceModel precedenceModel1=precedenceModels.get(i);
                                name.setText(extraName);
                                Log.i(TAG, "here we are: "+precedenceModel1.getFinalPrice());
                                situation.setText(precedenceModel1.getSituation());
                                bVol.setText(precedenceModel1.getbVol());
                                comp.setText(precedenceModel1.getComp());
                                indust.setText(precedenceModel1.getIndust());

                                update.setText(precedenceModel1.getUpdate());
                                num.setText(precedenceModel1.getNumberVariable());
                                latest.setText(precedenceModel1.getLatestPrice());
                                vol.setText(precedenceModel1.getTurnoverVariable());
                                finalP.setText(precedenceModel1.getFinalPrice());
                                val.setText(precedenceModel1.getValueVariable());
                                permitted.setText(precedenceModel1.getPermitted());
                                first.setText(precedenceModel1.getFirst());
                                period.setText(precedenceModel1.getPeriod());
                                yester.setText(precedenceModel1.getYesterday());

                                maxW.setText(precedenceModel1.getMaxW());
                                aveValueW.setText(precedenceModel1.getAveValueW());
                                minW.setText(precedenceModel1.getMinW());
                                aveVolW.setText(precedenceModel1.getAveVolW());

                                maxM.setText(precedenceModel1.getMaxM());
                                aveValueM.setText(precedenceModel1.getAveValueM());
                                minM.setText(precedenceModel1.getMinM());
                                aveVolM.setText(precedenceModel1.getAveVolM());

                                max3.setText(precedenceModel1.getMax3m());
                                aveValue3.setText(precedenceModel1.getAveValue3m());
                                min3.setText(precedenceModel1.getMin3m());
                                aveVol3.setText(precedenceModel1.getAveVol3m());

                                max6.setText(precedenceModel1.getMax6m());
                                aveValue6.setText(precedenceModel1.getAveValue6m());
                                min6.setText(precedenceModel1.getMin6m());
                                aveVol6.setText(precedenceModel1.getAveVol6m());

                                maxY.setText(precedenceModel1.getMaxY());
                                aveValueY.setText(precedenceModel1.getAveValueY());
                                minY.setText(precedenceModel1.getMinY());
                                aveVolY.setText(precedenceModel1.getAveVolY());

                            }
                        }
                        Log.i(TAG, "onSuccess: "+precedenceModels);


                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: "+e);
                    }


                });

    }
}