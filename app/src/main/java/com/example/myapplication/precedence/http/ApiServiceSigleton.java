package com.example.myapplication.precedence.http;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiServiceSigleton {
    private static ApiService apiService;

    public static ApiService getInstance(){
        if (apiService==null){
            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl("http://192.168.1.35/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService=retrofit.create(ApiService.class);
        }
        return apiService;
    }
}
