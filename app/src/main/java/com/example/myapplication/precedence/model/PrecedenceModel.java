package com.example.myapplication.precedence.model;

public class PrecedenceModel {
    public String name;

    public String finalPrice;
    public String latestPrice;

    public String turnoverVariable;
    public String valueVariable;
    public String numberVariable;

    public String profitMargin;

    private String situation;
    private String bVol;
    private String comp;
    private String indust;

    private String update;
    private String permitted;
    private String first;
    private String period;
    private String yesterday;

    private String maxW;
    private String aveValueW;
    private String minW;
    private String aveVolW;

    private String maxM;
    private String aveValueM;
    private String minM;
    private String aveVolM;

    private String max3m;
    private String aveValue3m;
    private String min3m;
    private String aveVol3m;

    private String max6m;
    private String aveValue6m;
    private String min6m;
    private String aveVol6m;

    private String maxY;
    private String aveValueY;
    private String minY;
    private String aveVolY;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getLatestPrice() {
        return latestPrice;
    }

    public void setLatestPrice(String latestPrice) {
        this.latestPrice = latestPrice;
    }

    public String getTurnoverVariable() {
        return turnoverVariable;
    }

    public void setTurnoverVariable(String turnoverVariable) {
        this.turnoverVariable = turnoverVariable;
    }

    public String getValueVariable() {
        return valueVariable;
    }

    public void setValueVariable(String valueVariable) {
        this.valueVariable = valueVariable;
    }

    public String getNumberVariable() {
        return numberVariable;
    }

    public void setNumberVariable(String numberVariable) {
        this.numberVariable = numberVariable;
    }

    public String getProfitMargin() {
        return profitMargin;
    }

    public void setProfitMargin(String profitMargin) {
        this.profitMargin = profitMargin;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public void setbVol(String bVol) {
        this.bVol = bVol;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public void setIndust(String indust) {
        this.indust = indust;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public void setPermitted(String permitted) {
        this.permitted = permitted;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setYesterday(String yesterday) {
        this.yesterday = yesterday;
    }

    public void setMaxW(String maxW) {
        this.maxW = maxW;
    }

    public void setAveValueW(String aveValueW) {
        this.aveValueW = aveValueW;
    }

    public void setMinW(String minW) {
        this.minW = minW;
    }

    public void setAveVolW(String aveVolW) {
        this.aveVolW = aveVolW;
    }

    public void setMaxM(String maxM) {
        this.maxM = maxM;
    }

    public void setAveValueM(String aveValueM) {
        this.aveValueM = aveValueM;
    }

    public void setMinM(String minM) {
        this.minM = minM;
    }

    public void setAveVolM(String aveVolM) {
        this.aveVolM = aveVolM;
    }

    public void setMax3m(String max3m) {
        this.max3m = max3m;
    }

    public void setAveValue3m(String aveValue3m) {
        this.aveValue3m = aveValue3m;
    }

    public void setMin3m(String min3m) {
        this.min3m = min3m;
    }

    public void setAveVol3m(String aveVol3m) {
        this.aveVol3m = aveVol3m;
    }

    public void setMax6m(String max6m) {
        this.max6m = max6m;
    }

    public void setAveValue6m(String aveValue6m) {
        this.aveValue6m = aveValue6m;
    }

    public void setMin6m(String min6m) {
        this.min6m = min6m;
    }

    public void setAveVol6m(String aveVol6m) {
        this.aveVol6m = aveVol6m;
    }

    public void setMaxY(String maxY) {
        this.maxY = maxY;
    }

    public void setAveValueY(String aveValueY) {
        this.aveValueY = aveValueY;
    }

    public void setMinY(String minY) {
        this.minY = minY;
    }

    public void setAveVolY(String aveVolY) {
        this.aveVolY = aveVolY;
    }

    public String getSituation() {
        return situation;
    }

    public String getbVol() {
        return bVol;
    }

    public String getComp() {
        return comp;
    }

    public String getIndust() {
        return indust;
    }

    public String getUpdate() {
        return update;
    }

    public String getPermitted() {
        return permitted;
    }

    public String getFirst() {
        return first;
    }

    public String getPeriod() {
        return period;
    }

    public String getYesterday() {
        return yesterday;
    }

    public String getMaxW() {
        return maxW;
    }

    public String getAveValueW() {
        return aveValueW;
    }

    public String getMinW() {
        return minW;
    }

    public String getAveVolW() {
        return aveVolW;
    }

    public String getMaxM() {
        return maxM;
    }

    public String getAveValueM() {
        return aveValueM;
    }

    public String getMinM() {
        return minM;
    }

    public String getAveVolM() {
        return aveVolM;
    }

    public String getMax3m() {
        return max3m;
    }

    public String getAveValue3m() {
        return aveValue3m;
    }

    public String getMin3m() {
        return min3m;
    }

    public String getAveVol3m() {
        return aveVol3m;
    }

    public String getMax6m() {
        return max6m;
    }

    public String getAveValue6m() {
        return aveValue6m;
    }

    public String getMin6m() {
        return min6m;
    }

    public String getAveVol6m() {
        return aveVol6m;
    }

    public String getMaxY() {
        return maxY;
    }

    public String getAveValueY() {
        return aveValueY;
    }

    public String getMinY() {
        return minY;
    }

    public String getAveVolY() {
        return aveVolY;
    }
}
