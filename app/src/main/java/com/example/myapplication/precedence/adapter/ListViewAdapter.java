package com.example.myapplication.precedence.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.precedence.model.PrecedenceModel;

import java.util.List;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.viewHolder> {

    private List<PrecedenceModel> precedenceArray;
    PrecedenceModel precedenceModel;
    private Context context;
    private OnStockTypeListener onStockTypeListener;

    int varCode;
    int priceCode;

    public ListViewAdapter(  List<PrecedenceModel> precedenceArray, Context context,int priceCode,int varCode,OnStockTypeListener onStockTypeListener) {
        this.context = context;
        this.precedenceArray = precedenceArray;
        this.priceCode=priceCode;
        this.varCode=varCode;
        this.onStockTypeListener=onStockTypeListener;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View layoutInflater= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_row,parent,false);



        if (viewType==0) {
            layoutInflater.setBackgroundColor(0xFFF3F2F8);
        }else {
            layoutInflater.setBackgroundColor(0xFFCDC8DC);
        }
        return new  viewHolder(layoutInflater,onStockTypeListener);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.onBind(precedenceArray.get(position));
        Log.i(TAG, "onBindViewHolder: "+precedenceArray.get(position));
    }

    @Override
    public int getItemCount() {
        return precedenceArray.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView name;
        public TextView price;
        public TextView var;
        public TextView profit;
        OnStockTypeListener onStockTypeListener;

    public viewHolder(@NonNull View itemView,OnStockTypeListener onStockTypeListener) {
        super(itemView);
        this.onStockTypeListener=onStockTypeListener;
        itemView.setOnClickListener(this);

        name=itemView.findViewById(R.id.sampleRow_tv_name);
        price=itemView.findViewById(R.id.sampleRow_tv_price);
        var=itemView.findViewById(R.id.sampleRow_tv_var);
        profit=itemView.findViewById(R.id.sampleRow_tv_profit);
    }
    public void onBind(PrecedenceModel precedenceModel){
        name.setText(precedenceModel.getName());
        switch(varCode) {
            case 0:
                var.setText(precedenceModel.getValueVariable());
                break;
            case 1:
                var.setText(precedenceModel.getTurnoverVariable());
                break;
            case 2:
                var.setText(precedenceModel.getNumberVariable());
                break;

        }
        switch(priceCode) {
            case 0:
                price.setText(precedenceModel.getLatestPrice());
                break;
            case 1:
                price.setText(precedenceModel.getFinalPrice());
                break;
        }
        profit.setText(precedenceModel.getProfitMargin());
    }

        @Override
        public void onClick(View view) {
            onStockTypeListener.onTypeClick(getAdapterPosition());
        }
    }
    public interface OnStockTypeListener{
        void onTypeClick(int position);
    }
}
