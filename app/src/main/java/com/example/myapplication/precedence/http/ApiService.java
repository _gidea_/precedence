package com.example.myapplication.precedence.http;

import com.example.myapplication.precedence.model.PrecedenceModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("database/{directory}")
    Single<List<PrecedenceModel>> getStocks(@Path("directory") String name);

}
