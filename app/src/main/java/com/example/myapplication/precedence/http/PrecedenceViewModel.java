package com.example.myapplication.precedence.http;

import com.example.myapplication.precedence.model.PrecedenceModel;

import java.util.List;

import io.reactivex.Single;

public class PrecedenceViewModel {
    private ApiService apiService;
    private String directory;

    public PrecedenceViewModel(ApiService apiService,String directory){
        this.apiService=apiService;
        this.directory=directory;
    }

    public Single<List<PrecedenceModel>> getStocks(){
        return apiService.getStocks(directory);
    }

}
