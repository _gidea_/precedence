package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.precedence.adapter.ListViewAdapter;
import com.example.myapplication.precedence.http.ApiServiceSigleton;
import com.example.myapplication.precedence.http.PrecedenceViewModel;
import com.example.myapplication.precedence.model.PrecedenceModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    private ListViewAdapter listViewAdapter;
    private List<PrecedenceModel> precedenceModelsArray;

    int varCode;
    int priceCode;

    final String TAG="ma_ac";

    Disposable disposable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView exchangeTv=findViewById(R.id.ma_tv_exchange);
        TextView varTv=findViewById(R.id.ma_tv_variables);

        recyclerView=findViewById(R.id.ma_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));


        exchangeTv.setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("pick a type")
                    .setItems(R.array.pick_price, (dialog, which) -> {
                        String TAG="sds";
                        Log.i(TAG, "onClick: "+which);
                        priceCode=which;

                        switch(which) {
                            case 0:
                                exchangeTv.setText("آخرین قیمت");
                                break;
                            case 1:
                                exchangeTv.setText("قیمت پایانی");
                                break;
                            }
                        listViewAdapter = new ListViewAdapter(precedenceModelsArray, MainActivity.this,priceCode,varCode, new ListViewAdapter.OnStockTypeListener() {
                            @Override
                            public void onTypeClick(int position) {

                                Intent intent=new Intent(MainActivity.this, SecondActivity.class);
                                intent.putExtra("name",precedenceModelsArray.get(position).getName());
                                startActivity(intent);
                            }
                        });
                        recyclerView.setAdapter(listViewAdapter);
                    });
            builder.create().show();

        });

        varTv.setOnClickListener(view -> {

            Log.i(TAG, "onClick: "+priceCode);
            Log.i(TAG, "onClick: "+precedenceModelsArray);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("pick a type")
                    .setItems(R.array.pick_var, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String TAG="sds";
                            Log.i(TAG, "onClick: "+which);
                            varCode=which;

                            switch(which) {
                                case 0:
                                    varTv.setText("ارزش معاملات");
                                    break;
                                case 1:
                                    varTv.setText("حجم معاملات");
                                    break;
                                case 2:
                                    varTv.setText("تعداد معاملات");
                                    break;

                            }

                            listViewAdapter = new ListViewAdapter(precedenceModelsArray, MainActivity.this,priceCode,varCode, new ListViewAdapter.OnStockTypeListener() {
                                @Override
                                public void onTypeClick(int position) {
                                    Intent intent=new Intent(MainActivity.this, SecondActivity.class);
                                    intent.putExtra("name",precedenceModelsArray.get(position).getName().toString());
                                    startActivity(intent);
                                }
                            });
                            recyclerView.setAdapter(listViewAdapter);
                        }
                    });
            builder.create().show();

        });


        PrecedenceViewModel precedenceViewModel=new PrecedenceViewModel(ApiServiceSigleton.getInstance(),"precedence.json");


        precedenceViewModel.getStocks().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<PrecedenceModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onSuccess(List<PrecedenceModel> precedenceModels) {

                        precedenceModelsArray=precedenceModels;
                        Log.i(TAG, "onSuccess: "+precedenceModels);

                        listViewAdapter = new ListViewAdapter(precedenceModelsArray, MainActivity.this,0,0, new ListViewAdapter.OnStockTypeListener() {
                            @Override
                            public void onTypeClick(int position) {
                                PrecedenceModel precedenceModel=precedenceModels.get(position);
                                Log.i(TAG, "onTypeClick: "+precedenceModel);
                                Intent intent=new Intent(MainActivity.this, SecondActivity.class);
                                intent.putExtra("name",precedenceModelsArray.get(position).getName().toString());
                                startActivity(intent);
                            }
                        });
                        recyclerView.setAdapter(listViewAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: "+e);
                    }


                });


    }
}